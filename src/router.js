import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
//import About from "./views/About.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/quotes",
      name: "quotes",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/getQuote.vue")
    },
    {
      path: "/submit",
      name: "submit",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/subQuote.vue")
    },
    {
      path: "/delete",
      name: "delete",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/delQuote.vue")
    },
    {
      path: "/update",
      name: "update",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/updQuote.vue")
    }
  ]
});
